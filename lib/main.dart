import 'package:flutter/material.dart';
import 'Home.dart';

void main() => runApp(MaterialApp(
      home: Home(),
      theme: ThemeData(
          hintColor: Colors.green[900],
          highlightColor: Colors.green[900],
          primaryColor: Colors.white,
          fontFamily: 'FiraCode'),
    ));
